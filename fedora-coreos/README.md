# NVIDIA Driver container for Fedora CoreOS

### Prerequisites

One needs to add nvidia-container-runtime-hook to docker.
Do it by creating file `/etc/systemd/system/docker.service.d/override.conf`,
and restarting docker:

```bash
sudo mkdir -p /etc/systemd/system/docker.service.d

sudo tee /etc/systemd/system/docker.service.d/override.conf <<EOF
[Unit]
After=docker.socket network-online.target firewalld.service nvidia-driver.service
Requires=docker.socket nvidia-driver.service
Wants=network-online.target
[Service]
Environment=PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/run/nvidia/driver/usr/bin
ExecStartPre=/usr/bin/sleep 90
ExecStart=
ExecStart=/usr/bin/dockerd --host=fd:// --add-runtime=nvidia=/run/nvidia/driver/usr/bin/nvidia-container-runtime --init-path /usr/libexec/docker/docker-init --userland-proxy-path /usr/libexec/docker/docker-proxy
EOF

ln -s /run/nvidia/driver/etc/nvidia-container-runtime/config.toml /etc/nvidia-container-runtime/config.toml

sudo systemctl daemon-reload
sudo systemctl restart docker

# Make sure the runtime is added
docker info | grep -i "runtime"
```

Do it *BEFORE* running driver.

### Building

```bash
DRIVER_VERSION=460.32.03
docker build --build-arg KERNEL_VERSION=$(uname -r) --build-arg ${DRIVER_VERSION} -t nvidia-driver-container:${DRIVER_VERSION}-$(uname -r) .
```

### Running nvidia-driver-container

```bash
docker run -d --name nvidia-driver --privileged --pid=host -v /run/nvidia:/run/nvidia:shared nvidia-driver-container:${DRIVER_VERSION}-$(uname -r) --accept-license

docker logs -f nvidia-driver # See example logs at the bootom of this file
```

### Link nvidia-container config file
```bash
ln -s /run/nvidia/driver/etc/nvidia-container-runtime/config.toml etc/nvidia-container-runtime/config.toml
```

### Veryfy installation
```bash
docker run --rm -it --runtime=nvidia --gpus all ffmpeg:11.2.1 nvidia-smi
```

```bash
sudo /run/nvidia/driver/usr/bin/nvidia-container-cli -k -r /run/nvidia/driver -d /dev/tty info # see logs at the bootom of this file
```

```bash
GPU=$(docker exec -it nvidia-driver nvidia-smi --query-gpu=uuid --format=csv,noheader | tail -1)
if [ "$(echo ${GPU} | wc -l)" = "0" ]; then
    echo "No nvidia gpu found"
    exit 1
fi

FPS="25"
GOP="50"

VP0B="6M"
VP1B="3M"
VP2B="1M"

VP0H="1080"
VP1H="720"
VP2H="480"

set -o xtrace
docker run --rm -it --runtime=nvidia ffmpeg:11.2.1 ffmpeg \
    -hwaccel_device 0 \
    -hide_banner \
    -y \
    -v verbose \
    -re \
    -vsync 1 \
    -flags +global_header \
    -fflags +genpts \
    -f lavfi -i "smptehdbars=size=hd1080:rate=${FPS}" \
    -f lavfi -i "sine=frequency=1000" \
    -filter_complex "[0:v:0]fps=fps=25:round=near,split=3[vp0in][vp1in][vp2in];\
    [vp0in]hwupload_cuda,scale_npp=w=trunc(oh*a/2)*2:h=${VP0H}:interp_algo=lanczos[vp0out];\
    [vp1in]hwupload_cuda,scale_npp=w=trunc(oh*a/2)*2:h=${VP1H}:interp_algo=lanczos[vp1out];\
    [vp2in]hwupload_cuda,scale_npp=w=trunc(oh*a/2)*2:h=${VP2H}:interp_algo=lanczos[vp2out]" \
    -b:v:0 ${VP0B} -minrate:v:0 ${VP0B} -maxrate:v:0 ${VP0B} -c:v:0 h264_nvenc -profile:v:0 main -rc:v:0 cbr -level:v:0 4.2 -r:v:0 ${FPS} -g:v:0 ${GOP} \
    -b:v:1 ${VP1B} -minrate:v:1 ${VP1B} -maxrate:v:1 ${VP1B} -c:v:1 h264_nvenc -profile:v:1 main -rc:v:1 cbr -level:v:1 4.2 -r:v:1 ${FPS} -g:v:1 ${GOP} \
    -b:v:2 ${VP2B} -minrate:v:2 ${VP2B} -maxrate:v:2 ${VP2B} -c:v:2 h264_nvenc -profile:v:2 main -rc:v:2 cbr -level:v:2 4.2 -r:v:2 ${FPS} -g:v:2 ${GOP} \
    -c:a:0 aac -ac:a:0 2 -ar:a:0 48000 -b:a:0 192k \
    -map "[vp0out]" -map "[vp1out]" -map "[vp2out]" -map 1:a:0 \
    -f tee '[select=\'\''v:0,a:0\'\'':f=null]|[select=\'\''v:1,a:0\'\'':f=null]|[select=\'\''v:2,a:0\'\'':f=null]'
```


## Output from nvidia-container-cli

```bash
sudo /run/nvidia/driver/usr/bin/nvidia-container-cli -k -r /run/nvidia/driver -d /dev/tty info

-- WARNING, the following logs are for debugging purposes only --

I0322 09:32:27.953123 142096 nvc.c:372] initializing library context (version=1.3.3, build=bd9fc3f2b642345301cb2e23de07ec5386232317)
I0322 09:32:27.953178 142096 nvc.c:346] using root /run/nvidia/driver/
I0322 09:32:27.953188 142096 nvc.c:347] using ldcache /etc/ld.so.cache
I0322 09:32:27.953195 142096 nvc.c:348] using unprivileged user 65534:65534
I0322 09:32:27.953221 142096 nvc.c:389] attempting to load dxcore to see if we are running under Windows Subsystem for Linux (WSL)
I0322 09:32:27.953372 142096 nvc.c:391] dxcore initialization failed, continuing assuming a non-WSL environment
I0322 09:32:27.958621 142098 nvc.c:274] loading kernel module nvidia
I0322 09:32:27.958755 142098 nvc.c:278] running mknod for /dev/nvidiactl
I0322 09:32:27.958984 142098 nvc.c:282] running mknod for /dev/nvidia0
I0322 09:32:27.959016 142098 nvc.c:282] running mknod for /dev/nvidia1
I0322 09:32:27.959043 142098 nvc.c:282] running mknod for /dev/nvidia2
I0322 09:32:27.959069 142098 nvc.c:282] running mknod for /dev/nvidia3
I0322 09:32:27.959095 142098 nvc.c:282] running mknod for /dev/nvidia4
I0322 09:32:27.959121 142098 nvc.c:282] running mknod for /dev/nvidia5
I0322 09:32:27.959146 142098 nvc.c:286] running mknod for all nvcaps in /dev/nvidia-caps
I0322 09:32:27.966054 142098 nvc.c:214] running mknod for /dev/nvidia-caps/nvidia-cap1 from /proc/driver/nvidia/capabilities/mig/config
I0322 09:32:27.966203 142098 nvc.c:214] running mknod for /dev/nvidia-caps/nvidia-cap2 from /proc/driver/nvidia/capabilities/mig/monitor
I0322 09:32:27.971061 142098 nvc.c:292] loading kernel module nvidia_uvm
I0322 09:32:27.971108 142098 nvc.c:296] running mknod for /dev/nvidia-uvm
I0322 09:32:27.971205 142098 nvc.c:301] loading kernel module nvidia_modeset
I0322 09:32:27.971230 142098 nvc.c:305] running mknod for /dev/nvidia-modeset
I0322 09:32:27.971514 142099 driver.c:101] starting driver service
I0322 09:32:27.976851 142096 nvc_info.c:680] requesting driver information with ''
I0322 09:32:27.978395 142096 nvc_info.c:169] selecting /usr/lib64/vdpau/libvdpau_nvidia.so.460.32.03
I0322 09:32:27.978500 142096 nvc_info.c:169] selecting /usr/lib64/libnvoptix.so.460.32.03
I0322 09:32:27.978574 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-tls.so.460.32.03
I0322 09:32:27.978626 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-rtcore.so.460.32.03
I0322 09:32:27.978681 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-ptxjitcompiler.so.460.32.03
I0322 09:32:27.978754 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-opticalflow.so.460.32.03
I0322 09:32:27.978830 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-opencl.so.460.32.03
I0322 09:32:27.978883 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-ngx.so.460.32.03
I0322 09:32:27.978934 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-ml.so.460.32.03
I0322 09:32:27.979006 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-ifr.so.460.32.03
I0322 09:32:27.979081 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-glvkspirv.so.460.32.03
I0322 09:32:27.979130 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-glsi.so.460.32.03
I0322 09:32:27.979182 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-glcore.so.460.32.03
I0322 09:32:27.979237 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-fbc.so.460.32.03
I0322 09:32:27.979314 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-encode.so.460.32.03
I0322 09:32:27.979386 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-eglcore.so.460.32.03
I0322 09:32:27.979456 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-compiler.so.460.32.03
I0322 09:32:27.979509 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-cfg.so.460.32.03
I0322 09:32:27.979583 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-cbl.so.460.32.03
I0322 09:32:27.979638 142096 nvc_info.c:169] selecting /usr/lib64/libnvidia-allocator.so.460.32.03
I0322 09:32:27.979714 142096 nvc_info.c:169] selecting /usr/lib64/libnvcuvid.so.460.32.03
I0322 09:32:27.979825 142096 nvc_info.c:169] selecting /usr/lib64/libcuda.so.460.32.03
I0322 09:32:27.979917 142096 nvc_info.c:169] selecting /usr/lib64/libGLX_nvidia.so.460.32.03
I0322 09:32:27.979973 142096 nvc_info.c:169] selecting /usr/lib64/libGLESv2_nvidia.so.460.32.03
I0322 09:32:27.980026 142096 nvc_info.c:169] selecting /usr/lib64/libGLESv1_CM_nvidia.so.460.32.03
I0322 09:32:27.980080 142096 nvc_info.c:169] selecting /usr/lib64/libEGL_nvidia.so.460.32.03
I0322 09:32:27.980146 142096 nvc_info.c:169] selecting /usr/lib/vdpau/libvdpau_nvidia.so.460.32.03
I0322 09:32:27.980212 142096 nvc_info.c:169] selecting /usr/lib/libnvidia-tls.so.460.32.03
I0322 09:32:27.980265 142096 nvc_info.c:169] selecting /usr/lib/libnvidia-ptxjitcompiler.so.460.32.03
I0322 09:32:27.980341 142096 nvc_info.c:169] selecting /usr/lib/libnvidia-opticalflow.so.460.32.03
I0322 09:32:27.980415 142096 nvc_info.c:169] selecting /usr/lib/libnvidia-opencl.so.460.32.03
I0322 09:32:27.980477 142096 nvc_info.c:169] selecting /usr/lib/libnvidia-ml.so.460.32.03
I0322 09:32:27.980551 142096 nvc_info.c:169] selecting /usr/lib/libnvidia-ifr.so.460.32.03
I0322 09:32:27.980625 142096 nvc_info.c:169] selecting /usr/lib/libnvidia-glvkspirv.so.460.32.03
I0322 09:32:27.980673 142096 nvc_info.c:169] selecting /usr/lib/libnvidia-glsi.so.460.32.03
I0322 09:32:27.980726 142096 nvc_info.c:169] selecting /usr/lib/libnvidia-glcore.so.460.32.03
I0322 09:32:27.980779 142096 nvc_info.c:169] selecting /usr/lib/libnvidia-fbc.so.460.32.03
I0322 09:32:27.980855 142096 nvc_info.c:169] selecting /usr/lib/libnvidia-encode.so.460.32.03
I0322 09:32:27.980927 142096 nvc_info.c:169] selecting /usr/lib/libnvidia-eglcore.so.460.32.03
I0322 09:32:27.980984 142096 nvc_info.c:169] selecting /usr/lib/libnvidia-compiler.so.460.32.03
I0322 09:32:27.981036 142096 nvc_info.c:169] selecting /usr/lib/libnvidia-allocator.so.460.32.03
I0322 09:32:27.981111 142096 nvc_info.c:169] selecting /usr/lib/libnvcuvid.so.460.32.03
I0322 09:32:27.981188 142096 nvc_info.c:169] selecting /usr/lib/libcuda.so.460.32.03
I0322 09:32:27.981266 142096 nvc_info.c:169] selecting /usr/lib/libGLX_nvidia.so.460.32.03
I0322 09:32:27.981321 142096 nvc_info.c:169] selecting /usr/lib/libGLESv2_nvidia.so.460.32.03
I0322 09:32:27.981375 142096 nvc_info.c:169] selecting /usr/lib/libGLESv1_CM_nvidia.so.460.32.03
I0322 09:32:27.981431 142096 nvc_info.c:169] selecting /usr/lib/libEGL_nvidia.so.460.32.03
W0322 09:32:27.981459 142096 nvc_info.c:350] missing library libnvidia-fatbinaryloader.so
W0322 09:32:27.981469 142096 nvc_info.c:354] missing compat32 library libnvidia-cfg.so
W0322 09:32:27.981480 142096 nvc_info.c:354] missing compat32 library libnvidia-fatbinaryloader.so
W0322 09:32:27.981490 142096 nvc_info.c:354] missing compat32 library libnvidia-ngx.so
W0322 09:32:27.981500 142096 nvc_info.c:354] missing compat32 library libnvidia-rtcore.so
W0322 09:32:27.981510 142096 nvc_info.c:354] missing compat32 library libnvoptix.so
W0322 09:32:27.981520 142096 nvc_info.c:354] missing compat32 library libnvidia-cbl.so
I0322 09:32:27.981915 142096 nvc_info.c:276] selecting /usr/bin/nvidia-smi
I0322 09:32:27.981946 142096 nvc_info.c:276] selecting /usr/bin/nvidia-debugdump
I0322 09:32:27.981977 142096 nvc_info.c:276] selecting /usr/bin/nvidia-persistenced
I0322 09:32:27.982007 142096 nvc_info.c:276] selecting /usr/bin/nvidia-cuda-mps-control
I0322 09:32:27.982037 142096 nvc_info.c:276] selecting /usr/bin/nvidia-cuda-mps-server
I0322 09:32:27.982077 142096 nvc_info.c:438] listing device /dev/nvidiactl
I0322 09:32:27.982087 142096 nvc_info.c:438] listing device /dev/nvidia-uvm
I0322 09:32:27.982097 142096 nvc_info.c:438] listing device /dev/nvidia-uvm-tools
I0322 09:32:27.982106 142096 nvc_info.c:438] listing device /dev/nvidia-modeset
I0322 09:32:27.982155 142096 nvc_info.c:317] listing ipc /run/nvidia-persistenced/socket
W0322 09:32:27.982180 142096 nvc_info.c:321] missing ipc /tmp/nvidia-mps
I0322 09:32:27.982190 142096 nvc_info.c:745] requesting device information with ''
I0322 09:32:27.988821 142096 nvc_info.c:628] listing device /dev/nvidia0 (GPU-fab4c608-8bcc-09e7-5579-48a404f83409 at 00000000:3b:00.0)
I0322 09:32:27.995394 142096 nvc_info.c:628] listing device /dev/nvidia1 (GPU-e1b4ad2c-6ac1-fc4b-2459-68c4197fa7f6 at 00000000:3c:00.0)
I0322 09:32:28.002070 142096 nvc_info.c:628] listing device /dev/nvidia2 (GPU-28b3dd70-ff8b-5000-571e-b1c727fc95be at 00000000:5e:00.0)
I0322 09:32:28.008828 142096 nvc_info.c:628] listing device /dev/nvidia3 (GPU-ec44ebe7-5cbd-33da-88a9-65f20cd50024 at 00000000:86:00.0)
I0322 09:32:28.015695 142096 nvc_info.c:628] listing device /dev/nvidia4 (GPU-9da5ba78-b1bc-6424-e8c0-95f2fb27d948 at 00000000:87:00.0)
I0322 09:32:28.022659 142096 nvc_info.c:628] listing device /dev/nvidia5 (GPU-d1ad896c-1bb5-5bc1-6827-54f69e402d46 at 00000000:d8:00.0)
NVRM version:   460.32.03
CUDA version:   11.2

Device Index:   0
Device Minor:   0
Model:          Quadro P2000
Brand:          Quadro
GPU UUID:       GPU-fab4c608-8bcc-09e7-5579-48a404f83409
Bus Location:   00000000:3b:00.0
Architecture:   6.1

Device Index:   1
Device Minor:   1
Model:          Quadro P2000
Brand:          Quadro
GPU UUID:       GPU-e1b4ad2c-6ac1-fc4b-2459-68c4197fa7f6
Bus Location:   00000000:3c:00.0
Architecture:   6.1

Device Index:   2
Device Minor:   2
Model:          Quadro P2000
Brand:          Quadro
GPU UUID:       GPU-28b3dd70-ff8b-5000-571e-b1c727fc95be
Bus Location:   00000000:5e:00.0
Architecture:   6.1

Device Index:   3
Device Minor:   3
Model:          Quadro P2000
Brand:          Quadro
GPU UUID:       GPU-ec44ebe7-5cbd-33da-88a9-65f20cd50024
Bus Location:   00000000:86:00.0
Architecture:   6.1

Device Index:   4
Device Minor:   4
Model:          Quadro P2000
Brand:          Quadro
GPU UUID:       GPU-9da5ba78-b1bc-6424-e8c0-95f2fb27d948
Bus Location:   00000000:87:00.0
Architecture:   6.1

Device Index:   5
Device Minor:   5
Model:          Quadro P2000
Brand:          Quadro
GPU UUID:       GPU-d1ad896c-1bb5-5bc1-6827-54f69e402d46
Bus Location:   00000000:d8:00.0
Architecture:   6.1
I0322 09:32:28.022887 142096 nvc.c:427] shutting down library context
I0322 09:32:28.024309 142099 driver.c:156] terminating driver service
I0322 09:32:28.024803 142096 driver.c:196] driver service terminated successfully
```

### Logs from proper driver run (docker logs nvidia-driver)
```
+ set -eu
+ KERNEL_VERSION=5.8.17-200.fc32.x86_64
+ RUN_DIR=/run/nvidia
+ PID_FILE=/run/nvidia/nvidia-driver.pid
+ DRIVER_VERSION=460.32.03
+ KOJI_BASE_URL=https://kojipkgs.fedoraproject.org
+ '[' 2 -eq 0 ']'
+ command=init
+ shift
+ case "${command}" in
++ getopt -l accept-license -o a -- --accept-license
+ options=' --accept-license --'
+ '[' 0 -ne 0 ']'
+ eval set -- ' --accept-license --'
++ set -- --accept-license --
+ ACCEPT_LICENSE=
++ uname -r
+ KERNEL_VERSION=5.8.17-200.fc32.x86_64
+ PRIVATE_KEY=
+ PACKAGE_TAG=
+ for opt in ${options}
+ case "$opt" in
+ ACCEPT_LICENSE=yes
+ shift 1
+ for opt in ${options}
+ case "$opt" in
+ shift
+ break
+ '[' 0 -ne 0 ']'
+ init
+ echo -e '\n========== NVIDIA Software Installer ==========\n'
+ echo -e 'Starting installation of NVIDIA driver version 460.32.03 for Linux kernel version 5.8.17-200.fc32.x86_64\n'
+ exec

========== NVIDIA Software Installer ==========

Starting installation of NVIDIA driver version 460.32.03 for Linux kernel version 5.8.17-200.fc32.x86_64

+ flock -n 3
+ echo 113617
+ trap 'echo '\''Caught signal'\''; exit 1' HUP INT QUIT PIPE TERM
+ trap _shutdown EXIT
+ _unload_driver
+ rmmod_args=()
+ local rmmod_args
+ local nvidia_deps=0
+ local nvidia_refs=0
+ local nvidia_uvm_refs=0
+ local nvidia_modeset_refs=0
+ echo 'Stopping NVIDIA persistence daemon...'
Stopping NVIDIA persistence daemon...
+ '[' -f /var/run/nvidia-persistenced/nvidia-persistenced.pid ']'
Unloading NVIDIA driver kernel modules...
+ echo 'Unloading NVIDIA driver kernel modules...'
+ '[' -f /sys/module/nvidia_modeset/refcnt ']'
+ nvidia_modeset_refs=0
+ rmmod_args+=("nvidia-modeset")
+ (( ++nvidia_deps ))
+ '[' -f /sys/module/nvidia_uvm/refcnt ']'
+ nvidia_uvm_refs=0
+ rmmod_args+=("nvidia-uvm")
+ (( ++nvidia_deps ))
+ '[' -f /sys/module/nvidia/refcnt ']'
+ nvidia_refs=2
+ rmmod_args+=("nvidia")
+ '[' 2 -gt 2 ']'
+ '[' 0 -gt 0 ']'
+ '[' 0 -gt 0 ']'
+ '[' 3 -gt 0 ']'
+ rmmod nvidia-modeset nvidia-uvm nvidia
+ return 0
+ _unmount_rootfs
+ echo 'Unmounting NVIDIA driver rootfs...'
Unmounting NVIDIA driver rootfs...
+ findmnt -r -o TARGET
+ grep /run/nvidia/driver
+ umount -l -R /run/nvidia/driver
++ uname -r
+ '[' 5.8.17-200.fc32.x86_64 == 5.8.17-200.fc32.x86_64 ']'
+ _create_driver_package
+ local pkg_name=nvidia-modules-5.8.17
+ local nvidia_sign_args=
+ local nvidia_modeset_sign_args=
+ local nvidia_uvm_sign_args=
+ trap 'make -s -j SYSSRC=/lib/modules/5.8.17-200.fc32.x86_64/build clean > /dev/null' EXIT
+ echo 'Compiling NVIDIA driver kernel modules...'
+ cd /usr/src/nvidia-460.32.03/kernel
Compiling NVIDIA driver kernel modules...
+ make -s -j SYSSRC=/lib/modules/5.8.17-200.fc32.x86_64/build nv-linux.o nv-modeset-linux.o
scripts/Makefile.lib:8: 'always' is deprecated. Please use 'always-y' instead
/usr/src/nvidia-460.32.03/kernel/nvidia-drm/nvidia-drm-modeset.c: In function '__will_generate_flip_event':
/usr/src/nvidia-460.32.03/kernel/nvidia-drm/nvidia-drm-modeset.c:96:23: warning: unused variable 'primary_plane' [-Wunused-variable]
   96 |     struct drm_plane *primary_plane = crtc->primary;
      |                       ^~~~~~~~~~~~~
+ echo 'Relinking NVIDIA driver kernel modules...'
+ rm -f nvidia.ko nvidia-modeset.ko
Relinking NVIDIA driver kernel modules...
+ ld -d -r -o nvidia.ko ./nv-linux.o ./nvidia/nv-kernel.o_binary
+ ld -d -r -o nvidia-modeset.ko ./nv-modeset-linux.o ./nvidia-modeset/nv-modeset-kernel.o_binary
+ '[' -n '' ']'
+ echo 'Building NVIDIA driver package nvidia-modules-5.8.17...'
Building NVIDIA driver package nvidia-modules-5.8.17...
+ ../mkprecompiled --pack nvidia-modules-5.8.17 --description 5.8.17-200.fc32.x86_64 --proc-mount-point /proc --driver-version 460.32.03 --kernel-interface nv-linux.o --linked-module-name nvidia.ko --core-object-name nvidia/nv-kernel.o_binary --target-directory . --kernel-interface nv-modeset-linux.o --linked-module-name nvidia-modeset.ko --core-object-name nvidia-modeset/nv-modeset-kernel.o_binary --target-directory . --kernel-module nvidia-uvm.ko --target-directory .
+ mkdir -p precompiled
+ mv nvidia-modules-5.8.17 precompiled
++ make -s -j SYSSRC=/lib/modules/5.8.17-200.fc32.x86_64/build clean
+ _install_driver
+ install_args=()
+ local install_args
+ echo 'Installing NVIDIA driver kernel modules...'
+ cd /usr/src/nvidia-460.32.03
+ rm -rf /lib/modules/5.8.17-200.fc32.x86_64/video
Installing NVIDIA driver kernel modules...
+ '[' yes = yes ']'
+ install_args+=("--accept-license")
+ IGNORE_CC_MISMATCH=1
+ nvidia-installer --kernel-module-only --no-drm --ui=none --no-nouveau-check --no-cc-version-check --accept-license

WARNING: The nvidia-drm module will not be installed. As a result, DRM-KMS will not function with this installation of the NVIDIA driver.


ERROR: Unable to open 'kernel/dkms.conf' for copying (No such file or directory)


Welcome to the NVIDIA Software Installer for Unix/Linux

Detected 16 CPUs online; setting concurrency level to 16.
Installing NVIDIA driver version 460.32.03.
A precompiled kernel interface for kernel '5.8.17-200.fc32.x86_64' has been found here: ./kernel/precompiled/nvidia-modules-5.8.17.
Kernel module linked successfully.
Kernel module linked successfully.
Kernel module unpacked successfully.
Unable to determine if Secure Boot is enabled: No such file or directory
Kernel messages:
[169065.298399] audit: type=1700 audit(1616355278.731:23337): dev=veth3471d49 prom=0 old_prom=256 auid=4294967295 uid=0 gid=0 ses=4294967295
[169084.820172] docker0: port 1(vethdc5e20c) entered blocking state
[169084.835907] docker0: port 1(vethdc5e20c) entered disabled state
[169084.851435] device vethdc5e20c entered promiscuous mode
[169084.865993] audit: type=1700 audit(1616355298.286:23338): dev=vethdc5e20c prom=256 old_prom=0 auid=4294967295 uid=0 gid=0 ses=4294967295
[169085.318925] eth0: renamed from veth4f8bd7e
[169085.341927] IPv6: ADDRCONF(NETDEV_CHANGE): vethdc5e20c: link becomes ready
[169085.357729] docker0: port 1(vethdc5e20c) entered blocking state
[169085.372339] docker0: port 1(vethdc5e20c) entered forwarding state
[169085.422293] nvidia-modeset: Unloading
[169085.462908] nvidia-uvm: Unloaded the UVM driver.
[169085.484848] nvidia-nvlink: Unregistered the Nvlink Core, major device number 236
[169106.747011] nvidia-nvlink: Nvlink Core is being initialized, major device number 511
[169106.747813] nvidia 0000:3b:00.0: vgaarb: changed VGA decodes: olddecodes=none,decodes=none:owns=none
[169106.948993] nvidia 0000:3c:00.0: vgaarb: changed VGA decodes: olddecodes=none,decodes=none:owns=none
[169107.148811] nvidia 0000:5e:00.0: vgaarb: changed VGA decodes: olddecodes=none,decodes=none:owns=none
[169107.348948] nvidia 0000:86:00.0: vgaarb: changed VGA decodes: olddecodes=none,decodes=none:owns=none
[169107.548846] nvidia 0000:87:00.0: vgaarb: changed VGA decodes: olddecodes=none,decodes=none:owns=none
[169107.749832] nvidia 0000:d8:00.0: vgaarb: changed VGA decodes: olddecodes=none,decodes=none:owns=none
[169107.949721] NVRM: loading NVIDIA UNIX x86_64 Kernel Module  460.32.03  Sun Dec 27 19:00:34 UTC 2020
[169107.975420] nvidia-uvm: Loaded the UVM driver, major device number 509.
[169107.980172] nvidia-modeset: Loading NVIDIA Kernel Mode Setting Driver for UNIX platforms  460.32.03  Sun Dec 27 18:51:11 UTC 2020
[169107.983353] nvidia-modeset: Unloading
[169108.011832] nvidia-uvm: Unloaded the UVM driver.
[169108.023753] nvidia-nvlink: Unregistered the Nvlink Core, major device number 511
Installing 'NVIDIA Accelerated Graphics Driver for Linux-x86_64' (460.32.03):
  Installing: [##############################] 100%
Driver file installation is complete.
Running post-install sanity check:
  Checking: [##############################] 100%
Post-install sanity check passed.
Running runtime sanity check:
  Checking: [##############################] 100%
Runtime sanity check passed.

Installation of the kernel module for the NVIDIA Accelerated Graphics Driver for Linux-x86_64 (version 460.32.03) is now complete.

+ _load_driver
+ echo 'Loading IPMI kernel module...'
+ modprobe ipmi_msghandler
Loading IPMI kernel module...
Loading NVIDIA driver kernel modules...
+ echo 'Loading NVIDIA driver kernel modules...'
+ modprobe -a nvidia nvidia-uvm nvidia-modeset
+ echo 'Starting NVIDIA persistence daemon...'
+ nvidia-persistenced --persistence-mode
Starting NVIDIA persistence daemon...
+ _mount_rootfs
+ echo 'Mounting NVIDIA driver rootfs...'
+ mount --make-runbindable /sys
Mounting NVIDIA driver rootfs...
+ mount --make-private /sys
+ mkdir -p /run/nvidia/driver
+ mount --rbind / /run/nvidia/driver
Change device files security context for selinux comptaibilty
+ echo 'Change device files security context for selinux comptaibilty'
+ chcon -R -t container_file_t /run/nvidia/driver/dev
Done, now waiting for signal
+ echo 'Done, now waiting for signal'
+ trap 'echo '\''Caught signal'\''; _shutdown && { kill 120129; exit 0; }' HUP INT QUIT PIPE TERM
+ trap - EXIT
+ sleep infinity
+ true
+ wait 120129
```
